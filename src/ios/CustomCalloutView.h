#import <UIKit/UIKit.h>
@import Mapbox;
 
@interface CustomCalloutView : UIView <MGLCalloutView>
- (instancetype)initColor:(UIColor *)color;
@end
